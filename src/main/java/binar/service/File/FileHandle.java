package binar.service.File;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

public abstract class FileHandle {
    protected String sourcePath;
    protected String outPath1;
    protected String outPath2;

    public FileHandle(){
        String absolutePath = new java.io.File("").getAbsolutePath();
        sourcePath = absolutePath + "/public/data/data_sekolah.csv";
        outPath1 = absolutePath +  "/public/output/freq_out.txt";
        outPath2 = absolutePath + "/public/output/stat_out.txt";
    }

    public String getOutPath1() {
        return outPath1;
    }

    public String getOutPath2() {
        return outPath2;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public abstract void readFile() throws FileNotFoundException;
    public abstract void generateFile(double mean, double median, double mode);

    public abstract void generatedFreqsFile(HashMap<Double, Double> freqData);

    @Override
    public String toString() {
        return "FileHandle{" +
                "sourcePath='" + sourcePath + '\'' +
                ", outPath1='" + outPath1 + '\'' +
                ", outPath2='" + outPath2 + '\'' +
                '}';
    }
}
