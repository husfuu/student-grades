package binar.view;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class MenuError extends Menu {

    private final FileNotFoundException notFoundError;
    private final String errorMessage;
    public MenuError(FileNotFoundException notFoundError){
        this.errorMessage = "File not found!";
        this.notFoundError = notFoundError;
    }

    public FileNotFoundException getNotFoundError() {
        return notFoundError;
    }

    @Override
    public void body() {
        System.out.println("Error messages: " + errorMessage);
        System.out.println("");
        System.out.println(
        "⠀⠀⠀⠀⠀⠀⢰⠊⠀⣰⡼⣿⣿⣿⣿⣄⠈⠻⣿⣷⡄⠙⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠈⣧⡴⠋⢶⡋⠁⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⠈⠓⣤⠿⣻⣿⣿⣿⣿⣿⣷⣄⠈⠻⣿⡷⠋⠁⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠘⣮⡠⠊⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⠀⠀⢩⣷⣿⣿⣿⣿⣿⣿⣿⣿⣷⠶⠋⠀⠀⠀⠀⠀⠀⠈⡟⠻⢿⣿⡿⣿⣿⣿⣿⣿⠛⣿⣿⣿⣀⣼⣿⣤⣸⣟⠁⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⠀⠀⡾⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⣷⣶⣾⣿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡗⢸⡄⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⠀⢰⣽⣿⣿⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢸⡇⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⠀⣞⣿⣿⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀⠀⣀⡀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⢻⣿⣿⣿⡏⣿⣿⣿⣿⣿⣿⣿⢈⡇⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠐⡙⠇⢀⠀⠀⠀⠀⢸⣿⣿⣿⣿⡿⣿⠀⣿⡿⣻⠁⢻⣿⣿⣿⣿⣿⣿⡀⡇⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠘⠌⢮⣧⠀⠀⠀⢸⡟⣏⢹⢿⠀⣏⠀⠘⣇⣽⠀⠘⣿⣿⣿⣿⣿⣿⣇⢧⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⡟⣥⣄⢹⣿⡷⠀⠀⠀⣀⣀⣀⡀⠦⢭⣧⡀⠀⣸⠻⢻⡼⢸⣷⢇⣠⡶⢿⣏⡀⠀⢸⣿⣿⣿⣿⣿⣿⢸⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⢀⣿⣿⣿⡟⢻⣿⠘⣆⠙⡆⣠⣾⠿⠛⠛⠛⠛⠻⣷⣼⣻⡶⠛⠀⠈⣧⡈⡿⢛⣵⡾⠛⠛⠛⠻⣧⣿⣿⣿⣿⣿⣿⣸⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⠀⣾⣿⣿⣿⡇⢸⣿⡆⡜⠂⣷⢻⣇⠀⠀⠀⣠⠔⠲⣄⠙⣏⠁⠀⠀⠀⠘⠅⢵⡿⠋⡄⢤⡀⠀⠀⢈⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣤⠿⢿⣿⡜⣜⠎⠻⢦⣀⠀⠻⠶⠿⢹⠀⠈⠀⠀⠀⠀⠀⠘⣾⠀⡞⠷⢾⡇⠀⣠⠞⢁⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀\n" +
        "⠀⠀⠀⢀⣾⣿⡟⢠⣿⣿⣿⣦⠀⠁⠙⠈⠛⠊⠉⠊⠝⠓⠒⠚⠛⠒⠦⢀⠀⣠⣤⡀⠤⢦⡠⠴⠤⠬⠖⠛⠁⠀⣾⣿⣿⣿⣿⣿⡟⡄⠀⠀⠀⠀\n" +
        "⠀⠀⠀⣼⣿⣿⠱⣸⣿⣿⣿⣿⣿⣦⣄⣀⡀⠀⣂⠀⠀⠀⠀⠀⠀⠀⡀⠈⠀⠈⣿⠓⣄⠳⠱⡈⠁⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣧⡇⠀⠀⠀⠀\n" +
        "⠀⠀⣰⢻⣿⠃⣤⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀⠈⠃⢀⡤⠄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠀⡠⡄⠀⢠⣿⣿⠟⣽⣿⢿⣿⣿⡇⠀⠀⠀⠀\n" +
        "⠀⢠⠏⣾⡟⠀⣿⣿⣿⣿⣿⣿⣏⣿⣿⣿⣿⣷⣤⡀⠈⠙⠒⠒⠲⣄⠀⠀⠀⠀⠀⠀⠀⢀⠠⠀⠀⠙⠁⢀⣼⡿⠁⣰⣿⣿⡾⢻⣿⣿⠀⠀⠀⠀\n" +
        "⠀⡼⣾⡿⠁⣸⣿⣷⣿⣟⣼⣟⡛⠻⣟⠿⣿⣿⣟⢽⣦⣄⠀⠀⠀⠈⠑⠲⠤⠤⠤⠶⠚⠉⠀⠀⠀⢀⣴⣿⣿⣅⣰⣿⣿⣷⠃⢸⣿⢹⠀⠀⠀⠀\n" +
        "⢰⣿⣿⠇⠀⣿⠿⠟⠛⠛⠛⠛⢿⣧⡄⢲⣌⢻⡄⠘⣷⣿⡳⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣤⣾⣿⣿⣿⣿⣿⣿⠟⠛⠛⠓⠼⣿⣯⡇⠀⠀⠀\n" +
        "⣾⣿⡿⢠⡾⠥⣤⣀⠀⠀⠀⠀⠀⠙⣿⣄⠹⣆⢻⡄⠻⣯⣷⡆⣟⠳⣤⣄⡀⣀⣠⣴⣶⠋⣿⣿⣿⣿⣿⣿⣿⣿⠛⠀⠀⠀⠀⠀⠈⢿⡇⠀⠀⠀\n" );
        System.out.println("==================================================================");
        System.out.println("1. Home");
        System.out.println("0. Exit");
        System.out.print("Choose: ");
    }
    @Override
    public void selectOption() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input){
            case "1":
                Menu menuHome = new MenuHome();
                menuHome.showMenu();
                break;
            case "0":
                scanner.close();
                break;
            default:
                Menu menuAlert = new MenuAlert();
                menuAlert.showMenu();
                break;
        }
    }
}
