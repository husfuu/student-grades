package binar.view;

import binar.service.File.FileHandleCSV;
import binar.service.Stats.StatsFreq;
import binar.service.Stats.StatsMean;
import binar.service.Stats.StatsMedian;
import binar.service.Stats.StatsMode;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class MenuHome extends Menu{
    @Override
    public void body() {
        System.out.println("1. Generate txt file for mean, media, and mode");
        System.out.println("2. Generate txt file for frequencies");
        System.out.println("3. Generate both files");
        System.out.println("0. Exit");
        System.out.print("Choose: ");
    }

    @Override
    public void selectOption() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                try{
                    FileHandleCSV file = new FileHandleCSV();
                    file.readFile();
                    List<Double> data = file.getData();

                    StatsMean meanObj = new StatsMean(data);
                    meanObj.calc();
                    double mean = meanObj.getResult();

                    StatsMedian medianObj = new StatsMedian(data);
                    medianObj.calc();
                    double median = medianObj.getResult();

                    StatsMode modeObj = new StatsMode(data);
                    modeObj.calc();
                    double mode = modeObj.getResult();

                    file.generateFile(mean, median, mode);
                    Menu menuSuccess = new MenuSuccess();
                    menuSuccess.showMenu();
                }catch (FileNotFoundException notFoundError) {
                    Menu menuError = new MenuError(notFoundError);
                    menuError.showMenu();
                }
                break;
            case "2":
                try {
                    FileHandleCSV file = new FileHandleCSV();
                    file.readFile();
                    List<Double> data = file.getData();

                    StatsFreq freqObj = new StatsFreq(data);
                    freqObj.calc();
                    HashMap<Double, Double> freq = freqObj.getResult();

                    file.generatedFreqsFile(freq);

                    Menu menuSuccess= new MenuSuccess();
                    menuSuccess.showMenu();
                } catch (FileNotFoundException notFoundError) {
                    Menu menuError = new MenuError(notFoundError);
                    menuError.showMenu();
                }
                break;
            case "3":
                try {
                    FileHandleCSV file = new FileHandleCSV();
                    file.readFile();
                    List<Double> data = file.getData();

                    StatsMean meanObj = new StatsMean(data);
                    meanObj.calc();
                    double mean = meanObj.getResult();

                    StatsMedian medianObj = new StatsMedian(data);
                    medianObj.calc();
                    double median = medianObj.getResult();

                    StatsMode modeObj = new StatsMode(data);
                    modeObj.calc();
                    double mode = modeObj.getResult();

                    file.generateFile(mean, median, mode);

                    StatsFreq freqObj = new StatsFreq(data);
                    freqObj.calc();
                    HashMap<Double, Double> freq = freqObj.getResult();

                    file.generatedFreqsFile(freq);

                    Menu menuSuccess= new MenuSuccess();
                    menuSuccess.showMenu();
                }
                catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                }
            case "0":
                scanner.close();
                break;
            default:
                Menu menuAlert = new MenuAlert();
                menuAlert.showMenu();
                break;
        }
    }
}
