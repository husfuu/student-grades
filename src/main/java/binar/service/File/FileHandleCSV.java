package binar.service.File;

import java.io.*;
import java.util.*;

import com.github.freva.asciitable.AsciiTable;

public class FileHandleCSV extends FileHandle{
    private List<Double> data;

    public FileHandleCSV(){
        super();
    }

    public FileHandleCSV(String sourcePath){
        this.sourcePath = sourcePath;
    }

    public List<Double> getData() {
        return data;
    }

    @Override
    public void readFile() throws FileNotFoundException {
        String line;
            List<List<String>> data = new ArrayList<>();
        List<Double> arrayData = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(sourcePath));
            while ((line = br.readLine()) != null){
                String[] values = line.split(";");
                data.add(Arrays.asList(values));
            }

            data.forEach(row -> row.forEach(num -> {
                try{
                    arrayData.add(new Double(num));
                } catch (NumberFormatException ignored){}
            }));

        } catch (FileNotFoundException e){
            throw new FileNotFoundException("File not found");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.data = arrayData;
    }

    @Override
    public void generateFile(double mean, double median, double mode) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outPath2));
            writer.write("Berikut adalah hasil statistic data: \n");
            String[] headers = {"Stats", "Value"};
            String[][] body = {
                    {"Mean", Double.toString(mean)},
                    {"Median", Double.toString(median)},
                    {"Mode", Double.toString(mode)}
            };
            writer.write(AsciiTable.getTable(headers, body));
            writer.close();
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void generatedFreqsFile(HashMap<Double, Double> freqData) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outPath1));
            String[] headers = {"", "Value", "Frequency"};
            String[][] body = new String[freqData.size()][];
            int index = 0;

            for (Map.Entry<Double, Double> set: freqData.entrySet()) {
                String [] temp = new String[3];
                temp[0] = String.valueOf(index+1);
                temp[1] = String.valueOf(set.getKey());
                temp[2] = String.valueOf(set.getValue());
                // temp array add to body
                body[index] = temp;
                index += 1;
            }
            writer.write(AsciiTable.getTable(headers, body));
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

