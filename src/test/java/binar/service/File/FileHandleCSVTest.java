package binar.service.File;

import binar.service.Stats.StatsMean;
import binar.service.Stats.StatsMedian;
import binar.service.Stats.StatsMode;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

class FileHandleCSVTest {
    @Test
    @DisplayName("is file succesfully generated")
    @RepeatedTest(value=1)
    void testGeneratedFileFound(){
        FileHandleCSV file = new FileHandleCSV();
        try {
            file.readFile();
            List<Double> data = file.getData();

            StatsMean meanObj = new StatsMean(data);
            meanObj.calc();
            double mean = meanObj.getResult();

            StatsMedian medianObj = new StatsMedian(data);
            medianObj.calc();
            double median = medianObj.getResult();

            StatsMode modeObj = new StatsMode(data);
            modeObj.calc();
            double mode = modeObj.getResult();

            file.generateFile(mean, median, mode);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        String absolutePath = new java.io.File("").getAbsolutePath();
        String outPath2 = absolutePath + "/public/output/stat_out.txt";
        File generatedFile = new File(outPath2);

        boolean exists = generatedFile.exists();
        Assertions.assertTrue(exists);
    }

    @Test(expected = FileNotFoundException.class)
    @DisplayName("test when the data is not found")
    @RepeatedTest(value = 2)
    public void testFileNotFoundException(){
        String absolutePath = new java.io.File("").getAbsolutePath();
        String sourcePath = absolutePath + "/public/data_sekolah.csv";
        FileHandleCSV file = new FileHandleCSV(sourcePath);

        Exception exception = Assertions.assertThrows(FileNotFoundException.class, file::readFile);

        Assertions.assertEquals("File not found", exception.getMessage());
    }
}