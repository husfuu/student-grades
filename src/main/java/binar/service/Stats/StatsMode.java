package binar.service.Stats;

import java.util.List;
import java.util.Objects;

public class StatsMode implements IStats{
    private double result;
    private final List<Double> data;
    public StatsMode(List<Double> data) {
        this.data = data;
    }
    @Override
    public void calc() {
        double res = 0;
        int maxCount = 0;
        for (int i = 0; i < data.size(); i++) {
            int count = 0;
            for (Double datum : data) {
                if (Objects.equals(data.get(i), datum)) {
                    count += 1;
                }
            }
            if (count > maxCount){
                maxCount = count;
                res = data.get(i);
            }
        }
        this.result = res;
    }
    public double getResult() {
        return result;
    }
}
