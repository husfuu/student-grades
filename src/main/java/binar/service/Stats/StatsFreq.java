package binar.service.Stats;

import java.util.HashMap;
import java.util.List;

public class StatsFreq implements IStats{
    private final List<Double> data;
    private HashMap<Double, Double> result;

    public StatsFreq(List<Double> data){
        this.data = data;
    }
    
    public HashMap<Double, Double> getResult() {
        return result;
    }

    public void setResult(HashMap<Double, Double> result) {
        this.result = result;
    }

    @Override
    public void calc() {
        HashMap<Double, Double> freqs = new HashMap<Double, Double>();
        double max = 1;
        double temp = 0;

        for (int i = 0; i < data.size(); i++) {
            if (freqs.get(data.get(i)) != null){
                double count = freqs.get(data.get(i));
                count += 1;
                freqs.put(data.get(i), count);

                if (count > max){
                    max = count;
                    temp = data.get(i);
                }
            } else {
                freqs.put(data.get(i), 1.0);
            }
        }
        this.result = freqs;
    }
}
