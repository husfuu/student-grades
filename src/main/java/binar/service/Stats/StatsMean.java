package binar.service.Stats;

import java.util.List;
import java.util.OptionalDouble;

public class StatsMean implements IStats {
    private final List<Double> data;
    private Double result;

    public StatsMean (List<Double> data){
        this.data = data;
    }


    @Override
    public void calc() {

        OptionalDouble average = data.stream().mapToDouble(Double::doubleValue).average();

//        System.out.println(data.stream().mapToDouble(StatsMean::getData).average());

//        double sum = 0;
//        for (Double datum : data){
//            sum += datum;
//        }
//        this.result = sum / data.size();
        this.result = average.getAsDouble();
    }

    public double getResult(){
        return result;
    }
}
