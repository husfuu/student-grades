package binar.service.Stats;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class StatsMedian implements IStats {
    private final List<Double> data;
    private Double result;

    public StatsMedian (List<Double> data){
        this.data = data;
    }

    @Override
    public void calc() {
        Collections.sort(data);
//        Stream<Double> sortedData = data.stream().sorted();
        int num = data.size();
        double res;
        if (num % 2 == 1){
            res = data.get((num + 1));
        } else {
            res = data.get( ((num/2-1) + (num / 2 )) / 2 );
        }
        this.result = res;
    }

    public Double getResult(){
        return result;
    }
}
