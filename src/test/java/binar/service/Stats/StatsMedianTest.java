package binar.service.Stats;

import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatsMedianTest {
    public List<Double> getRandomData(int length){
        double min = 0;
        double max = 10;
        List<Double> arr =  new ArrayList<>();
        for (int i = 0; i < length; i++) {
            double rand_val = Math.floor(Math.random() * (max-min +1) + min);
            arr.add(rand_val);
        }
        return arr;
    }

    @Test
    @RepeatedTest(value = 5, name = "Repeating test range mode with random data test {currentRepetition} of {totalRepetitions}")
    public void testResultRange(){
        List<Double> data = getRandomData(10);
        StatsMedian medianObj = new StatsMedian(data);
        medianObj.calc();
        double median = medianObj.getResult();
        // positive case
        assertTrue(0 <= median && median <= 10);
        // negative case
        assertFalse(0 > median || median > 10);
    }
}