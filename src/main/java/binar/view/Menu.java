package binar.view;
import java.util.Scanner;
public abstract class Menu {
    protected Scanner scanner;

    public void header(){
        System.out.println("==========================================================================");
        System.out.println("Student Grade CLI App");
        System.out.println("==========================================================================");
    }

    public abstract void body();

    public abstract void selectOption();

    public void showMenu(){
        header();
        body();
        selectOption();
    }
}
